# DCEffectSound Framework1.0
If you need add some sound effect in your iOS app DCEffectSound framework can help you

![create-custom-ios-framework.png](https://bitbucket.org/repo/poqr7k/images/2859565056-create-custom-ios-framework.png)

#How to use
import DCEffectSound.framework into your project
![screenshot](http://diegocaridei.altervista.org/blog/wp-content/uploads/2013/10/import.png)
import the framework into the class

--#import <DCEffectSound/DCEffectSound.h>

## Play /stop
[DCSound Sound :@"code" :@"mp3" :@"play"];


Code = name_file
mp3 = type
play/stop = play/stop 